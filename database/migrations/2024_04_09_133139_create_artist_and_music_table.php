<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->dateTime('dob');
            $table->enum('gender', ['m', 'f','o']);
            $table->string('address');
            $table->year('first_release_year');
            $table->integer('no_of_albums_released');
            $table->timestamps();
        });

        Schema::create('musics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artist_id');
            $table->string('title');
            $table->string('album_name');
            $table->enum('genre', ['rnb', 'country','classic','rock','jazz'])->nullable();
            $table->timestamps();
            $table->foreign('artist_id')->references('id')->on('artists')->onUpdate('cascade')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('artists');
        Schema::dropIfExists('musics');
    }
};
