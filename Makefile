all:start

WEBBASH = docker exec -it webapplaravel bash
#SOLRBASH = docker exec -it solr_server bash
DBBASH = docker exec -it db bash
UID = 1000 #$(shell id -u)
GID = 0 #$(shell id -g)
CURRENT_USER = test #$(shell whoami)
DOCKERENV = env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER}

define HELP
\n
command				|  		definition
====================================================================================
>> stop				|  stops the docker container.
>> start			|  starts the docker container.
>> clean			|  removes all the temp files, stops the docker and removes the docker images as well.
>> attach			|  attach the docker container for debug purposes.
>> db-bash			|  enter the DB docker Bash.
>> web-bash			|  enter the Web docker Bash.
>> solr-bash			|  enter the Solr docker Bash.
>> import-db			|  executes all sql files within temp/db directory
>> setup-project		|  creates the direcories required and helps to import dependencies
>> build-no-cache		|  builds the docker container without systems cache.
>> build-with-cache		|  builds the docker container with existing cache.

endef
export HELP

help:
	@echo "$$HELP"


build-with-cache:
	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker compose -f ./docker-compose.yml build

build-no-cache:
	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker compose -f ./docker-compose.yml build --no-cache

start:
	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker compose -f ./docker-compose.yml up -d

logs:
	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker compose -f ./docker-compose.yml logs -f

attach:
	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker compose attach app

stop:
	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker compose -f ./docker-compose.yml down


web-bash:
	$(WEBBASH)

db-bash:
	$(DBBASH)

#import-db:
#	env UID=${UID} GID=${GID} CURRENT_USER=${CURRENT_USER} docker exec -it mysql_server import_db.sh

#change-password-type-db:
#	$(DBBASH) mysql_changes.sh
#
#run-sql-scripts:
#	$(WEBBASH) ./bin/run_sql_scripts.sh
