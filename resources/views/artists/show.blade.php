@extends('master')

@section('master_content')
<div class="card">

    <div class="card-header"><a href="{{ route('artists.index') }}"> <i class="fas fa-arrow-left"></i></a> Artist Information </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">Name</div>
            <div class="col-md-6">{{$artist->name}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Date of Birth</div>
            <div class="col-md-6">{{$artist->dob}}</div>
        </div>

        <div class="row">
            <div class="col-md-6">Gender</div>
            <div class="col-md-6">{{$artist->gender}}</div>
        </div>

        <div class="row">
            <div class="col-md-6">Address</div>
            <div class="col-md-6">{{$artist->address}}</div>
        </div>

        <div class="row">
            <div class="col-md-6">Release Year</div>
            <div class="col-md-6">{{$artist->first_release_year}}</div>
        </div>

        <div class="row">
            <div class="col-md-6">Number of Album Released</div>
            <div class="col-md-6">{{$artist->no_of_albums_released}}</div>
        </div>
    </div>
</div>
@endsection
