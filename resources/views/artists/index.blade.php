@extends('master')

@section('master_content')
<div class="container">
    <p class="d-inline-flex gap-1">
        <a href="{{ route('artists.create') }}" class="btn btn-success">Add <i class="fa-solid fa-circle-plus"></i></a>
    </p>
</div>
<div class="card">
    <div class="card-header">{{ __('Artist') }}</a></div>

    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Release Year</th>
                    <th scope="col">No. of Albums</th>
                    <th scope="col">Songs</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if(count($artists)>0)
                    @foreach ($artists as $artist)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $artist->name }}</td>
                            <td>{{ $artist->first_release_year }}</td>
                            <td>{{ $artist->no_of_albums_released }}</td>
                            <td>{{ $artist->number_of_music }}</td>

                            <td>
                                <a href="{{ route('musics.index',$artist->id) }}" class="text-info" ><i class="fa-solid fa-circle-plus"></i></a>
                                <a href="{{ route('artists.show', $artist->id) }}" class="text-info" alt="view">  <i class="fas fa-eye"></i></a>
                                <a href="{{ route('artists.edit', $artist->id) }}" class="text-primary" alt="edit"><i class="fas fa-pencil"></i></a>
                                <form action="{{ route('artists.destroy', $artist->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <a class="text-danger" style="cursor: pointer" onclick="parentNode.submit();" alt="delete"><i class="fas fa-trash"></i></a>
                                  </form>
                            </td>

                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="8" class="text-center">No Data</td>
                </tr>
                @endif
            </tbody>
        </table>
        @if(count($artists)>0)

        <nav aria-label="Page navigation example" >
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="{{ route('artists.index',['current_page'=>($current_page==1?1:$current_page-1)]) }}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                @foreach ($visiblepages as $page)
                    <li class="page-item"><a class="page-link" href="{{ route('artists.index',['current_page'=>$page]) }}">{{$page}}</a></li>
                @endforeach
                <li class="page-item">
                    <a class="page-link" href="{{ route('artists.index',['current_page'=>($current_page==$last_page?$last_page:$current_page+1)]) }}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
        @endif
    </div>
</div>
@endsection
