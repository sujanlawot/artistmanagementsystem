@extends('master')

@section('master_content')
<div class="container">
    <p class="d-inline-flex gap-1">
        <a href="{{ route('users.create') }}" class="btn btn-success">Add <i class="fa-solid fa-circle-plus"></i></a>
    </p>

</div>
<div class="card">
    <div class="card-header">{{ __('User') }}</div>

    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Date of Birth</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Address</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if(count($users)>0)
                    @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $user->full_name }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->dob }}</td>
                            <td>{{ $user->gender }}</td>
                            <td>{{ $user->address }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a href="{{ route('users.show', $user->id) }}" class="text-info" alt="view">  <i class="fas fa-eye"></i></a>
                                <a href="{{ route('users.edit', $user->id) }}" class="text-primary" alt="edit"><i class="fas fa-pencil"></i></a>
                                <form action="{{ route('users.destroy', $user->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    {{-- <a href="javascript:;" onclick="parentNode.submit();"><%=n%></a> --}}
                                    <a class="text-danger" style="cursor: pointer" onclick="parentNode.submit();" alt="delete"><i class="fas fa-trash"></i></a>
                                    {{-- <button type="submit" class="btn btn-danger btn-sm">Delete</button> --}}
                                  </form>
                            </td>

                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="8" class="text-center">No Data</td>
                </tr>
                @endif
            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                {{-- rows.current_page == 1 ? 1 : rows.current_page - 1 --}}
                <li class="page-item">
                    <a class="page-link" href="{{ route('users.index',['current_page'=>($current_page==1?1:$current_page-1)]) }}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                @foreach ($visiblepages as $page)
                    <li class="page-item"><a class="page-link" href="{{ route('users.index',['current_page'=>$page]) }}">{{$page}}</a></li>
                @endforeach
                <li class="page-item">
                    <a class="page-link" href="{{ route('users.index',['current_page'=>($current_page==$last_page?$last_page:$current_page+1)]) }}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
@endsection
