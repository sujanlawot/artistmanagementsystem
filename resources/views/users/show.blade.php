@extends('master')

@section('master_content')
<div class="container">
    <p class="d-inline-flex gap-1">
        <a href="{{ route('users.index') }}" class="btn btn-primary">Back <i class="fa-solid fa-arrow-left text-white"></i></a>
    </p>

</div>
<div class="card">
    <div class="card-header"> User Information </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">Full Name</div>
            <div class="col-md-6">{{$user->full_name}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Email</div>
            <div class="col-md-6">{{$user->email}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Phone Number</div>
            <div class="col-md-6">{{$user->phone}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Date of Birth</div>
            <div class="col-md-6">{{$user->dob}}</div>
        </div>

        <div class="row">
            <div class="col-md-6">Gender</div>
            <div class="col-md-6">{{$user->gender}}</div>
        </div>

        <div class="row">
            <div class="col-md-6">Address</div>
            <div class="col-md-6">{{$user->address}}</div>
        </div>
    </div>
</div>
@endsection
