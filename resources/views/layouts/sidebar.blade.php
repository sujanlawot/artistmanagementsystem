<div class="list-group">
    @if(in_array(Route::currentRouteName(), ['users.index','users.create','users.store','users.show','users.edit','users.update','users.destroy']))
        <a href="{{ route('users.index') }}" class="list-group-item list-group-item-action active" aria-current="true">
            Users
        </a>
        <a href="{{ route('artists.index') }}" class="list-group-item list-group-item-action">Artists</a>
    @else
        <a href="{{ route('users.index') }}" class="list-group-item list-group-item-action" aria-current="true">
            Users
        </a>
        <a href="{{ route('artists.index') }}" class="list-group-item list-group-item-action active">Artists</a>
    @endif
</div>
