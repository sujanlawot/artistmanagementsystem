@extends('master')

@section('master_content')
<div class="container">
    <p class="d-inline-flex gap-1">
        <a href="{{ route('musics.index',$artist_id) }}" class="btn btn-primary">Back <i class="fa-solid fa-arrow-left text-white"></i></a>
    </p>

</div>
<div class="card">

    <div class="card-header"> Music Information </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-6">Title</div>
            <div class="col-md-6">{{$music->title}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Album Name</div>
            <div class="col-md-6">{{$music->album_name}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Genre</div>
            <div class="col-md-6">{{$music->genre}}</div>
        </div>
        <div class="row">
            <div class="col-md-6">Artist Name</div>
            <div class="col-md-6">{{$music->name}}</div>
        </div>

    </div>
</div>
@endsection
