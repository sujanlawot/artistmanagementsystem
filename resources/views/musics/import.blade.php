@extends('master')

@section('master_content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Import Music</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('musics.importMusics',$artist_id) }}" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <input class="form-control" type="file" id="formFile" name="import_csv_file">
                          </div>
                          <button type="submit" class="btn btn-primary">
                              {{ __('Import') }}
                          </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
