@extends('master')

@section('master_content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Music Edit</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('musics.update',['id'=>$music->id]) }}">
                        @csrf
                        @method('PUT')


                        <div class="row mb-3">
                            <label for="title" class="col-md-4 col-form-label text-md-end">Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title"  value="{{ $music->title }}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="album_name" class="col-md-4 col-form-label text-md-end">Album Name</label>

                            <div class="col-md-6">
                                <input id="album_name" type="text" class="form-control @error('album_name') is-invalid @enderror" name="album_name" value="{{ $music->album_name}}"  required autocomplete="album_name" autofocus>

                                @error('album_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="genre" class="col-md-4 col-form-label text-md-end">Genre</label>

                            <div class="col-md-6">
                                <select class="form-select" name="genre" aria-label="Default select example">
                                    <option {{ $music->genre == null ? 'selected' : '' }}>Select</option>
                                    <option value="rnb" {{ $music->genre == "rnb" ? 'selected' : '' }}>Rnb</option>
                                    <option value="country" {{ $music->genre == "country" ? 'selected' : '' }}>Country</option>
                                    <option value="classic" {{ $music->genre == "classic" ? 'selected' : '' }}>Classic</option>
                                    <option value="rock" {{ $music->genre == "rock" ? 'selected' : '' }}>Rock</option>
                                    <option value="jazz" {{ $music->genre == "jazz" ? 'selected' : '' }}>Jazz</option>
                                  </select>
                                @error('genre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
