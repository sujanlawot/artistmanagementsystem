@extends('master')

@section('master_content')
<div class="container">
    <p class="d-inline-flex gap-1">
        @if($artist_id!=null)
            <a href="{{ route('musics.create', $artist_id) }}" class="btn btn-success">Add <i class="fa-solid fa-circle-plus"></i></a>
            <a href="{{ route('musics.import',$artist_id) }}" class="btn btn-secondary">Import <i class="fa-solid fa-upload"></i></a>
            <a href="{{ route('musics.export',$artist_id) }}" class="btn btn-info">Export <i class="fa-solid fa-download text-white"></i></a>
        @else
        <a href="{{ route('musics.export') }}" class="btn btn-info">Export <i class="fa-solid fa-download text-white"></i></a>
        @endif

        <a href="{{ route('musics.template') }}" class="btn btn-info">Template <i class="fa-solid fa-download text-white"></i></a>
    </p>

</div>
<div class="card">

    <div class="card-header">{{ 'Music' }}</div>

    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Album Name</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if(count($musics)>0)
                    @foreach ($musics as $music)
                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $music->title }}</td>
                            <td>{{ $music->album_name }}</td>
                            <td>{{ $music->genre }}</td>
                            <td>
                                <a href="{{ route('musics.show', $music->id) }}" class="text-info" alt="view">  <i class="fas fa-eye"></i></a>
                                <a href="{{ route('musics.edit', $music->id) }}" class="text-primary" alt="edit"><i class="fas fa-pencil"></i></a>
                                <form action="{{ route('musics.destroy', $music->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <a class="text-danger" style="cursor: pointer" onclick="parentNode.submit();" alt="delete"><i class="fas fa-trash"></i></a>
                                  </form>
                            </td>

                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="8" class="text-center">No Data</td>
                </tr>
                @endif
            </tbody>
        </table>
        @if(count($musics)>0)

        <nav aria-label="Page navigation example" >
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="{{ route('musics.index',['current_page'=>($current_page==1?1:$current_page-1)]) }}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                @foreach ($visiblepages as $page)
                    <li class="page-item"><a class="page-link" href="{{ route('musics.index',['current_page'=>$page]) }}">{{$page}}</a></li>
                @endforeach
                <li class="page-item">
                    <a class="page-link" href="{{ route('musics.index',['current_page'=>($current_page==$last_page?$last_page:$current_page+1)]) }}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
        @endif
    </div>
</div>
@endsection
