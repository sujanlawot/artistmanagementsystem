<?php

use App\Http\Controllers\ArtistController;
use App\Http\Controllers\MusicController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Auth::routes();
Route::get('/users/create', UserController::class . '@create')->name('users.create');
Route::post('/users', UserController::class .'@store')->name('users.store');
Route::middleware(['auth'])->group(function () {
    Route::get('/', UserController::class .'@index')->name('users.index');
    Route::get('/users/{id}', UserController::class .'@show')->name('users.show');
    Route::get('/users/{id}/edit', UserController::class .'@edit')->name('users.edit');
    Route::put('/users/{id}', UserController::class .'@update')->name('users.update');
    Route::delete('/users/{id}', UserController::class .'@destroy')->name('users.destroy');

    Route::prefix('artists')->group(function () {
        Route::get('/', ArtistController::class .'@index')->name('artists.index');
        Route::get('create', ArtistController::class . '@create')->name('artists.create');
        Route::post('/', ArtistController::class .'@store')->name('artists.store');
        Route::get('{id}', ArtistController::class .'@show')->name('artists.show');
        Route::get('{id}/edit', ArtistController::class .'@edit')->name('artists.edit');
        Route::put('{id}', ArtistController::class .'@update')->name('artists.update');
        Route::delete('{id}', ArtistController::class .'@destroy')->name('artists.destroy');
    });

    Route::prefix('musics')->group(function () {
        Route::get('/{artist_id?}', MusicController::class .'@index')->name('musics.index');
        Route::get('/template',MusicController::class .'@template')->name('musics.template');
        Route::get('/export/{artist_id?}',MusicController::class .'@export')->name('musics.export');
        Route::get('/import/{artist_id}', MusicController::class .'@import')->name('musics.import');
        Route::post('/importHeader/{artist_id}', MusicController::class .'@importHeader')->name('musics.importHeader');
        Route::post('/importMusics/{artist_id}', MusicController::class .'@importMusics')->name('musics.importMusics');
        Route::get('/create/{artist_id}', MusicController::class . '@create')->name('musics.create');
        Route::post('/{artist_id}', MusicController::class .'@store')->name('musics.store');
        Route::get('{id}/show', MusicController::class .'@show')->name('musics.show');
        Route::get('{id}/edit', MusicController::class .'@edit')->name('musics.edit');
        Route::put('{id}', MusicController::class .'@update')->name('musics.update');
        Route::delete('{id}', MusicController::class .'@destroy')->name('musics.destroy');
    });



    });
