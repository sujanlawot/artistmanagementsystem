FROM php:8.2-fpm

# Arguments for user and UID
ARG user
ARG uid

# Install system dependencies and PHP extensions
RUN apt-get update && apt-get install -y \
    zip \
    libzip-dev \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure zip \
    && docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd xml zip

# Install Composer
COPY --from=composer:2.7.7 /usr/bin/composer /usr/bin/composer

# Create and configure the user
RUN useradd -G www-data,root -u $uid -d /home/$user $user \
    && mkdir -p /home/$user/.composer \
    && chown -R $user:$user /home/$user

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_20.x | bash - \
    && apt-get install -y nodejs



# Set the working directory
WORKDIR /var/www

COPY package.json package-lock.json /var/www

RUN npm install --no-cache

COPY . /var/www

# Switch to the non-root user
USER $user

# EXPOSE 9002

# # # CMD ["npm", "run", "dev", "--host", "0.0.0.0", "--port", "3001"]
# # CMD ["sh", "-c", "npm run dev -- --host 0.0.0.0 --port 3001"]
# CMD ["php-fpm"]




