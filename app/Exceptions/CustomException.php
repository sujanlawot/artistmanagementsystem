<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class CustomException extends Exception
{
     // Redefine the exception so message isn't optional
     public function __construct($message, $code = 400, Throwable $previous = null)
     {
         // make sure everything is assigned properly
         parent::__construct($message, $code, $previous);
     }

     // custom string representation of object
     public function __toString()
     {
         return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
     }
}
