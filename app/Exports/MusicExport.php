<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel as ExcelExcel;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MusicExport implements FromArray,WithHeadings,Responsable
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    private $fileName = 'template.csv';
    private $writerType = ExcelExcel::CSV;
    private $headers = [
        'Content-Type' => 'text/csv',
    ];
    private $template;
    private $artist_id;
    public function __construct($template=true,$artist_id=null)
    {
        $this->template=$template;
        $this->artist_id=$artist_id;
    }
    public function array(): array
    {
        if($this->template==true)
        {
            return [];
        }
        $condition="";
        if($this->artist_id!=null)
        {
            $condition="where a.id=$this->artist_id";
        }
        DB::statement('set @rownum=0');


        return ((array)(DB::select("select @rownum := @rownum + 1 as number,a.name,m.title,m.album_name,m.genre from artists a join musics m on m.artist_id=a.id $condition")));
    }
    public function headings(): array
    {
        if($this->template==true)
        {
            return [
                'SN',
                'Title',
                'Album Name',
                'Genre'
            ];
        }
        return [
            'SN',
            'Artist',
            'Title',
            'Album Name',
            'Genre'
        ];
    }
}
