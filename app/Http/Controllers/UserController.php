<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function visiblePageNumber($current_page, $max_visible_page, $last_page)
    {
        $start_page = max(1, $current_page - floor($max_visible_page / 2));
        $end_page = min($last_page, $start_page + $max_visible_page - 1);

        // Adjust start page if end page is at the end of the range
        $start_page = max(1, $end_page - $max_visible_page + 1);

        // Generate array of visible page numbers
        return range($start_page, $end_page);
    }


    public function index(Request $request)
    {
        $limits = !empty($request->limits) ? $request->limits : 3;
        $current_page = !empty($request->current_page) ? $request->current_page : 1;
        $count = DB::select("select count(*) as aggregate from users")[0]->aggregate;
        $data = DB::select("select CONCAT(firstname,' ',lastname) as full_name,concat(SUBSTRING(phone,1,1),'*********') as phone,concat(year(date(dob)),' ',MONTHNAME(date(dob)),' ',day(date(dob))) as dob,case when gender = 'm' then 'Male' when gender ='f' then 'Female' when gender='o' then 'Other' else '-' end as gender,address ,email,id from users limit :limit offset :offset", [
            'limit' => $limits,
            'offset' => ($current_page - 1) * $limits
        ]);

        $last_page = intval(ceil($count / $limits));

        $visiblepages = $this->visiblePageNumber($current_page, 3, $last_page);
        return view('users.index')->with(['users' => $data, 'i' => 1, 'limits' => $limits, 'current_page' => $current_page, 'count' => $count, 'visiblepages' => $visiblepages, 'last_page' => $last_page]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'max:500', 'confirmed'],
            'phone' => ['required', 'string', 'max:20'],
            'dob' => ['required', 'date'],
            'gender' => ['required'],
            'address' => ['required', 'max:255'],
        ]);

        DB::insert("INSERT into users(firstname,lastname,email,password,phone,dob,gender,address,created_at,updated_at) VALUE(:firstname,:lastname,:email,:password,:phone,:dob,:gender,:address,:created_at,:updated_at)", [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'dob' => $request->dob,
            'gender' => $request->gender,
            'address' => $request->address,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('users.index')
            ->with('success', 'user created successfully.');
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'firstname' => ['required', 'string', 'max:255'],
                'lastname' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:20'],
                'dob' => ['required', 'date'],
                'gender' => ['required'],
                'address' => ['required', 'max:255'],
            ]);


            DB::update("UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email, phone = :phone, dob = :dob, gender = :gender, address = :address, updated_at = :updated_at WHERE id = :id", [
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'phone' => $request->phone,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'address' => $request->address,
                'updated_at' => \Carbon\Carbon::now(),
                'id' => $id
            ]);

            return redirect()->route('users.index')
                ->with('success', 'user updated successfully.');
        } catch (Exception $e) {
            return redirect()->route('users.index')
                ->with('error', 'Something went wrong.');
        }
    }

    public function destroy($id)
    {
        try {

            $count = DB::delete('delete from users where id = ?', [$id]);
            if ($count != 1) {
                throw new Exception('User not found');
            }

            return redirect()->route('users.index')
                ->with('success', 'user deleted successfully');
        } catch (Exception $e) {
            return redirect()->route('users.index')
                ->with('error', 'Something went wrong.');
        }
    }

    public function create()
    {
        return view('users.create');
    }

    public function show($id)
    {
        try
        {
            $user=DB::select("select CONCAT(firstname,' ',lastname) as full_name,phone ,date(dob) as dob,case when gender = 'm' then 'Male' when gender ='f' then 'Female' when gender='o' then 'Other' else '-' end as gender,email,address from users where id=?",[$id]);
            if(count($user)!=1)
            {
                throw new Exception('Something went wrong');
            }
            return view('users.show')->with(['user' => $user[0]]);
        }
        catch(Exception $e)
        {
            return redirect()->route('users.index')
            ->with('error', 'Something went wrong.');
        }
    }

    public function edit($id)
    {
        try
        {
            $user=DB::select('select * from users where id=?',[$id]);
            if(count($user)!=1)
            {
                throw new Exception('Something went wrong');
            }
            return view('users.edit')->with(['user' => $user[0]]);
        }
        catch(Exception $e)
        {
            return redirect()->route('users.index')
            ->with('error', 'Something went wrong.');
        }
    }
}
