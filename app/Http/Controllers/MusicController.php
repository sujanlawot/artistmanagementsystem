<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Exports\MusicExport;
use App\Imports\MusicImport;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\HeadingRowImport;

class MusicController extends Controller
{
    public function visiblePageNumber($current_page, $max_visible_page, $last_page)
    {
        $start_page = max(1, $current_page - floor($max_visible_page / 2));
        $end_page = min($last_page, $start_page + $max_visible_page - 1);

        // Adjust start page if end page is at the end of the range
        $start_page = max(1, $end_page - $max_visible_page + 1);

        // Generate array of visible page numbers
        return range($start_page, $end_page);
    }
    public function index(Request $request)
    {
        $id=$request->artist_id??null;
        $limits = !empty($request->limits) ? $request->limits : 3;
        $current_page = !empty($request->current_page) ? $request->current_page : 1;
        $where_condition="";
        if($id!=null)
        {
            $where_condition ="where m.artist_id= $id";
        }
        $count = DB::select("select count(*) as aggregate from musics m $where_condition")[0]->aggregate;

        $data = DB::select("select a.id,m.artist_id,m.title,m.album_name,m.genre,a.name from musics m join artists a on a.id=m.artist_id $where_condition limit :limit offset :offset", [
            'limit' => $limits,
            'offset' => ($current_page - 1) * $limits
        ]);

        $last_page = intval(ceil($count / $limits));

        $visiblepages = $this->visiblePageNumber($current_page, 3, $last_page);
        return view('musics.index')->with(['musics' => $data, 'i' => 1, 'limits' => $limits, 'current_page' => $current_page, 'count' => $count, 'visiblepages' => $visiblepages, 'last_page' => $last_page,'artist_id'=>$id]);
    }

    public function import(Request $request)
    {
        return view('musics.import',['artist_id'=>$request->artist_id]);

    }

    public function template()
    {
        return new MusicExport();
    }

    public function export($artist_id=null)
    {
        return new MusicExport(false,$artist_id);
    }

    public function importHeader(Request $request)
    {

        $status=200;
        try{
            if(!$request->hasFile('import_csv_file'))
            {
                throw new CustomException("Please Upload file");
            }

            if($request->file('import_csv_file')->getClientOriginalExtension()!='csv')
            {
                throw new CustomException("Upload file must be csv template file");
            }


            $path=$request->file('import_csv_file')->getRealPath();
            $header=(new HeadingRowImport())->toArray($path,null,\Maatwebsite\Excel\Excel::CSV);

            $response=[
                'message'=>'Successfully Header Fetched',
                'data'=>$header[0][0]
            ];
        }
        catch(CustomException $e)
        {
            $status=400;
            $response=[
                'message'=>$e->getMessage(),
            ];
        }
        catch(Exception $e)
        {
            $status=400;
            $response=[
                'message'=>'Unable to Fetched Header',
                'error'=>$e->getMessage(),
                'trace'=>$e->getTrace()
            ];
        }
        return response()->json($response,$status);
    }

    public function importMusics(Request $request)
    {
        try
        {
            if(!$request->hasFile('import_csv_file'))
            {
                throw new CustomException("Please Upload file");
            }

            if($request->file('import_csv_file')->getClientOriginalExtension()!='csv')
            {
                throw new CustomException("Upload file must be csv template file");
            }

            $path=$request->file('import_csv_file')->getRealPath();
            (new MusicImport($request->artist_id))->import($path,null,\Maatwebsite\Excel\Excel::CSV);
            return redirect()->route('musics.index',['artist_id'=>$request->artist_id])->with('success','Artist Music Successfully Uploaded');
        }
        catch(CustomException $e)
        {
            return redirect()->route('musics.import',['artist_id'=>$request->artist_id])->with('error',$e->getMessage());
        }
        catch(Exception $e)
        {

            return redirect()->route('musics.import',['artist_id'=>$request->artist_id])->with('error','Something went wrong');
        }
    }

    public function show($id)
    {
        try
        {
            $music=DB::select('select m.title,m.album_name,m.genre,a.name from musics m join artists a on m.artist_id=a.id where m.id=?',[$id]);
            if(count($music)!=1)
            {
                throw new Exception('Something went wrong');
            }
            return view('musics.show')->with(['music' => $music[0],'artist_id'=>$id]);
        }
        catch(Exception $e)
        {
            return redirect()->route('musics.index',['artist_id'=>$id])
            ->with('error', 'Something went wrong.');
        }
    }

    public function create($artist_id)
    {
        return view('musics.create',['artist_id'=>$artist_id]);
    }

    public function store(Request $request,$artist_id)
    {
        try
        {
            $request->validate([
                'title' => ['required', 'max:255'],
                'album_name' => ['required', 'max:255'],
                'genre' => ['required'],
            ]);



            DB::insert("INSERT into musics(artist_id,album_name,genre,title,created_at,updated_at) VALUE(:artist_id,:album_name,:genre,:title,:created_at,:updated_at)", [
                'artist_id' => $artist_id,
                'album_name' => $request->album_name,
                'genre' => $request->genre,
                'title' => $request->title,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            return redirect()->route('musics.index')
                ->with('success', 'Music created successfully.');
        }
        catch(Exception $e)
        {
            return redirect()->route('musics.index')
            ->with('error', 'Something went wrong.');
        }
    }

    public function edit($id)
    {
        try
        {
            $music=DB::select('select * from musics where id=?',[$id]);
            if(count($music)!=1)
            {
                throw new Exception('Something went wrong');
            }
            return view('musics.edit')->with(['music' => $music[0]]);
        }
        catch(Exception $e)
        {
            return redirect()->route('musics.index',['artist_id'=>$id])
            ->with('error', 'Something went wrong.');
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'title' => ['required', 'max:255'],
                'album_name' => ['required', 'max:255'],
                'genre' => ['required'],
            ]);


            DB::update("UPDATE musics SET title = :title, album_name = :album_name, genre = :genre, updated_at = :updated_at WHERE id = :id", [
                'title' => $request->title,
                'album_name' => $request->album_name,
                'genre' => $request->genre,
                'updated_at' => Carbon::now(),
                'id' => $id
            ]);

            return redirect()->route('musics.index')
                ->with('success', 'Music updated successfully.');
        } catch (Exception $e) {
            return redirect()->route('musics.index')
                ->with('error', 'Something went wrong.');
        }
    }


    public function destroy($id)
    {
        try {

            $count = DB::delete('delete from musics where id = ?', [$id]);
            if ($count != 1) {
                throw new Exception('Music not found');
            }

            return redirect()->route('musics.index')
                ->with('success', 'Music deleted successfully');
        } catch (Exception $e) {
            return redirect()->route('musics.index')
                ->with('error', 'Something went wrong.');
        }
    }



}
