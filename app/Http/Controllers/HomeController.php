<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $limits = !empty($request->limits) ? $request->limits : 10;
        $page_number=!empty($request->page_number) ? $request->page_number : 1;
        $search = !empty($request->search) ? $request->search : '';
        $count=DB::select("select count(*) as aggregate from users")[0]->aggregate;
        if ($search != '') {
            $data=DB::select("select CONCAT(firstname,' ',lastname) as full_name,phone ,date(dob) as dob,case when gender = 'm' then 'Male' when gender ='f' then 'Female' when gender='o' then 'Other' else '-' end as gender ,address ,email,id from users where search like '%:search%' limit :limit offset :offset",[
                'limit'=>$limits,
                'offset'=>($page_number-1)*$limits,
                'search'=>$search
            ]);
        } else {
            $data=DB::select("select CONCAT(firstname,' ',lastname) as full_name,phone ,date(dob) as dob,case when gender = 'm' then 'Male' when gender ='f' then 'Female' when gender='o' then 'Other' else '-' end as gender,address ,email,id from users limit :limit offset :offset",[
                'limit'=>$limits,
                'offset'=>($page_number-1)*$limits
            ]);
        }
        return view('home')->with(['users'=>$data,'i'=>1]);
    }
}
