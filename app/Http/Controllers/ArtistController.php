<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArtistController extends Controller
{
    public function visiblePageNumber($current_page, $max_visible_page, $last_page)
    {
        $start_page = max(1, $current_page - floor($max_visible_page / 2));
        $end_page = min($last_page, $start_page + $max_visible_page - 1);

        // Adjust start page if end page is at the end of the range
        $start_page = max(1, $end_page - $max_visible_page + 1);

        // Generate array of visible page numbers
        return range($start_page, $end_page);
    }
    public function index(Request $request)
    {
        $limits = !empty($request->limits) ? $request->limits : 3;
        $current_page = !empty($request->current_page) ? $request->current_page : 1;
        $count = DB::select("select count(*) as aggregate from artists")[0]->aggregate;
        $data = DB::select("select 
     a.name,a.first_release_year,a.no_of_albums_released,a.id,count(m.id) as number_of_music from artists a left join musics m on m.artist_id=a.id group by a.id limit :limit offset :offset", [
            'limit' => $limits,
            'offset' => ($current_page - 1) * $limits
        ]);
        $last_page = intval(ceil($count / $limits));

        $visiblepages = $this->visiblePageNumber($current_page, 3, $last_page);
        return view('artists.index')->with(['artists' => $data, 'i' => 1, 'limits' => $limits, 'current_page' => $current_page, 'count' => $count, 'visiblepages' => $visiblepages, 'last_page' => $last_page]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'dob' => ['required', 'date'],
            'gender' => ['required'],
            'address' => ['required', 'max:255'],
            'first_release_year' => ['required'],
            'no_of_albums_released' => ['required','integer'],
        ]);


        DB::insert("INSERT into artists(name,dob,gender,address,first_release_year,no_of_albums_released,created_at,updated_at) VALUE(:name,:dob,:gender,:address,:first_release_year,:no_of_albums_released,:created_at,:updated_at)", [
            'name' => $request->name,
            'dob' => $request->dob,
            'gender' => $request->gender,
            'address' => $request->address,
            'first_release_year' => $request->first_release_year,
            'no_of_albums_released' => $request->no_of_albums_released,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('artists.index')
            ->with('success', 'Artist created successfully.');
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'dob' => ['required', 'date'],
                'gender' => ['required'],
                'address' => ['required', 'max:255'],
                'first_release_year' => ['required'],
                'no_of_albums_released' => ['required','integer'],
            ]);


            DB::update("UPDATE artists SET name = :name, dob = :dob, gender = :gender,address = :address,first_release_year = :first_release_year,no_of_albums_released = :no_of_albums_released, updated_at = :updated_at WHERE id = :id", [
                'name' => $request->name,
                'dob' => $request->dob,
                'gender' => $request->gender,
                'address' => $request->address,
                'first_release_year' => $request->first_release_year,
                'no_of_albums_released' => $request->no_of_albums_released,
                'updated_at' => Carbon::now(),
                'id' => $id
            ]);

            return redirect()->route('artists.index')
                ->with('success', 'Artist updated successfully.');
        } catch (Exception $e) {
            return redirect()->route('artists.index')
                ->with('error', 'Something went wrong.');
        }
    }

    public function destroy($id)
    {
        try {

            $count = DB::delete('delete from artists where id = ?', [$id]);
            if ($count != 1) {
                throw new Exception('Artist not found');
            }

            return redirect()->route('artists.index')
                ->with('success', 'Artist deleted successfully');
        } catch (Exception $e) {
            return redirect()->route('artists.index')
                ->with('error', 'Something went wrong.');
        }
    }

    public function create()
    {
        return view('artists.create');
    }

    public function show($id)
    {
        try
        {
            $artist=DB::select('select * from artists where id=?',[$id]);
            if(count($artist)!=1)
            {
                throw new Exception('Something went wrong');
            }
            return view('artists.show')->with(['artist' => $artist[0]]);
        }
        catch(Exception $e)
        {
            return redirect()->route('artists.index')
            ->with('error', 'Something went wrong.');
        }
    }

    public function edit($id)
    {
        try
        {
            $artist=DB::select('select * from artists where id=?',[$id]);
            if(count($artist)!=1)
            {
                throw new Exception('Something went wrong');
            }
            return view('artists.edit')->with(['artist' => $artist[0]]);
        }
        catch(Exception $e)
        {
            return redirect()->route('artists.index')
            ->with('error', 'Something went wrong.');
        }
    }
}
