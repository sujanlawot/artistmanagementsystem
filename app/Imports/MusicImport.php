<?php

namespace App\Imports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class MusicImport implements ToCollection,WithHeadingRow
{
    use Importable;
    /**
    * @param Collection $collection
    */

    private $artist_id;
    private $genre=['rnb','country','classic','rock','jazz'];

    public function __construct($artist_id)
    {
        $this->artist_id=$artist_id;
    }
    public function headingRow(): int
    {
        return 1;
    }

    public function collection(Collection $rows)
    {

        foreach ($rows as $row)
        {
            $row['genre']=in_array($row['genre'],$this->genre)?$row['genre']:null;
            if(!empty($row['title']) && !empty($row['album_name']))
            {
                DB::insert("INSERT into musics(artist_id,title,album_name,genre,created_at,updated_at) VALUE(:artist_id,:title,:album_name,:genre,:created_at,:updated_at)", [
                    'artist_id' => $this->artist_id,
                    'title' => $row['title'],
                    'album_name' => $row['album_name'],
                    'genre' => $row['genre'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }
}
